package ru.t1.karimov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model) throws AbstractException;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@Nullable String id) throws AbstractException;

    @Nullable
    M findOneByIndex(@NotNull Integer index) throws AbstractException;

    @Nullable
    M removeOne(@NotNull M model) throws AbstractEntityNotFoundException;

    @Nullable
    M removeOneById(@NotNull String id) throws AbstractFieldException;

    @Nullable
    M removeOneByIndex(@NotNull Integer index) throws AbstractFieldException;

    void removeAll();

    int getSize();

    boolean existsById(@NotNull String id);

}
