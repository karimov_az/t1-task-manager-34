package ru.t1.karimov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.IUserOwnedRepository;
import ru.t1.karimov.tm.api.service.IUserOwnedService;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.IndexIncorrectException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        model.setUserId(userId);
        return repository.add(userId, model);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator
    ) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return repository.findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException("Error! Index оut of bounds...");
        return repository.findOneByIndex(userId, index);
    }

    @Override
    public int getSize(@Nullable final String userId) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeOneById(userId, id);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException("Error! Index оut of bounds...");
        return repository.removeOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final String userId, @Nullable final M model) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        return repository.removeOne(userId, model);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeAll(userId);
    }

}
