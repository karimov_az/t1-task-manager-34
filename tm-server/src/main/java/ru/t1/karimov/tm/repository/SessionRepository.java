package ru.t1.karimov.tm.repository;

import ru.t1.karimov.tm.api.repository.ISessionRepository;
import ru.t1.karimov.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
