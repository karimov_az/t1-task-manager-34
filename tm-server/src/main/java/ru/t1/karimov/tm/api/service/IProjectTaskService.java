package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.exception.AbstractException;

public interface IProjectTaskService {

    void bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws AbstractException;

    void removeProjectById(@Nullable String userId, @Nullable String projectId) throws AbstractException;

    void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws AbstractException;

}
