package ru.t1.karimov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.task.TaskListRequest;
import ru.t1.karimov.tm.dto.response.task.TaskListResponse;
import ru.t1.karimov.tm.enumerated.TaskSort;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.entity.TaskNotFoundException;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASKS LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken(), sort);
        @NotNull final TaskListResponse response = getTaskEndpoint().listTask(request);

        @Nullable final List<Task> tasks = response.getTaskList();
        if (tasks == null) throw new TaskNotFoundException();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show list tasks.";
    }

}
