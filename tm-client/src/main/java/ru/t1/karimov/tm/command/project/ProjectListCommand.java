package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.project.ProjectListRequest;
import ru.t1.karimov.tm.dto.response.project.ProjectListResponse;
import ru.t1.karimov.tm.enumerated.ProjectSort;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken(), sort);
        @NotNull final ProjectListResponse response = getProjectEndpoint().listProject(request);
        @Nullable final List<Project> projects = response.getProjects();
        if (projects == null) throw new ProjectNotFoundException();
        int index = 1;
        for (@Nullable final Project project: projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName() + ": " + project.getDescription() + ", "
                    + Status.toName(project.getStatus()));
            index++;
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project list.";
    }

}
