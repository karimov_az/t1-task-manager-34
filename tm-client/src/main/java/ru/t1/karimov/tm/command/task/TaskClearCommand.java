package ru.t1.karimov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.request.task.TaskClearRequest;
import ru.t1.karimov.tm.exception.AbstractException;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASKS CLEAR]");
        @NotNull final TaskClearRequest request = new TaskClearRequest(getToken());
        getTaskEndpoint().clearTask(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

}
