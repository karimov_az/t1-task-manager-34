package ru.t1.karimov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.karimov.tm.dto.response.user.UserUpdateProfileResponse;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @Nullable final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        @NotNull final UserUpdateProfileResponse response = getUserEndpoint().updateUserProfile(request);
        @Nullable final User user = response.getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-update-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update profile of current user.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
