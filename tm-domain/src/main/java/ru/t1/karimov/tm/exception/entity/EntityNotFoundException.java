package ru.t1.karimov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;

public class EntityNotFoundException extends AbstractEntityNotFoundException {

    public EntityNotFoundException() {
        super("ERROR! Entity not found!");
    }

    public EntityNotFoundException(@NotNull String entity) {
        super("ERROR! Entity:" + entity + " not found!");
    }

}

